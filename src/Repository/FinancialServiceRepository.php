<?php

namespace App\Repository;

use App\Entity\FinancialService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FinancialService|null find($id, $lockMode = null, $lockVersion = null)
 * @method FinancialService|null findOneBy(array $criteria, array $orderBy = null)
 * @method FinancialService[]    findAll()
 * @method FinancialService[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FinancialServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FinancialService::class);
    }

    // /**
    //  * @return FinancialService[] Returns an array of FinancialService objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FinancialService
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
