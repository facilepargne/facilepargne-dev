<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EstablishmentRepository")
 */
class Establishment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $genre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FinancialService", mappedBy="establishment")
     */
    private $financialServices;

    public function __construct()
    {
        $this->services = new ArrayCollection();
        $this->financialServices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getGenre(): ?string
    {
        return $this->genre;
    }

    public function setGenre(string $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * @return Collection|FinancialService[]
     */
    public function getFinancialServices(): Collection
    {
        return $this->financialServices;
    }

    public function addFinancialService(FinancialService $financialService): self
    {
        if (!$this->financialServices->contains($financialService)) {
            $this->financialServices[] = $financialService;
            $financialService->setEstablishment($this);
        }

        return $this;
    }

    public function removeFinancialService(FinancialService $financialService): self
    {
        if ($this->financialServices->contains($financialService)) {
            $this->financialServices->removeElement($financialService);
            // set the owning side to null (unless already changed)
            if ($financialService->getEstablishment() === $this) {
                $financialService->setEstablishment(null);
            }
        }

        return $this;
    }
}
