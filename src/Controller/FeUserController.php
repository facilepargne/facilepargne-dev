<?php

namespace App\Controller;

use App\Entity\FeUser;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FeUserController extends AbstractController
{
    /**
     * @Route("/user/{slug}", name="user_show")
     */
    public function index(FeUser $user)
    {
        return $this->render('fe_user/index.html.twig', [
            'user' => $user,
        ]);
    }
}
