<?php

namespace App\Controller;

use App\Entity\FeUser;
use App\Form\AccountType;
use App\Entity\PasswordUpdate;
use App\Form\RegistrationType;
use App\Form\PasswordUpdateType;
use Symfony\Component\Form\FormError;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AccountController extends AbstractController
{
    /**
     * @Route("/login", name="account_login", methods={"GET", "POST"})
     */
    public function login(AuthenticationUtils $utils)
    {
        $error = $utils->getLastAuthenticationError();
        $lastUsername = $utils->getLastUsername();
        
        return $this->render('account/login.html.twig', [
            'hasError' => $error !== null,
            'username' => $lastUsername
        ]);
    }

    /**
     * @Route("/logout", name="account_logout")
     * @Security("is_granted('ROLE_USER')", message="Pourquoi vouloir se déconnecter si vous ne vous êtes pas connecté ?")
     */
    public function logout()
    {

    }

    /**
     * Permet d'afficher le formulaire d'inscription
     * 
     * @Route("/register", name="account_register")
     * 
     * @return Response
     */
    public function register(Request $request, ObjectManager $objectManager, UserPasswordEncoderInterface $encoder)
    {
        $user = new FeUser();

        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $objectManager->persist($user);
            $objectManager->flush();

            $this->addFlash(
                'success',
                "Votre compte a bien été créé ! Vous pouvez maintenant vous connecter !"
            );

            return $this->redirectToRoute('account_login');
        }

        return $this->render('account/registration.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Permet d'afficher et de traiter le formulaire de modification de profil.
     * 
     * @Route("/account/profile", name="account_profile")
     * @Security("is_granted('ROLE_USER')", message="Vous devez vous connecter pour accéder à cette page !")
     *
     * @return Response
     */
    public function profile(Request $request, ObjectManager $objectManager)
    {
        $user = $this->getUser();

        $form =$this->createForm(AccountType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $objectManager->persist($user);
            $objectManager->flush();

            $this->addFlash(
                'success',
                "Les données du profil ont été enregistrées avec succès !"
            );
        }
        
        return $this->render("account/profile.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * Permet de mettre à jour le mot de passe de l'utilisateur
     *
     * @Route("account/update-password", name="account_password")
     * @Security("is_granted('ROLE_USER')", message="Vous devez vous connecter pour accéder à cette page !")
     * 
     * @return Response
     */
    public function updatePassword(Request $request, ObjectManager $objectManager, UserPasswordEncoderInterface $encoder)
    {
        $passwordUpdate = new PasswordUpdate();
        $user = $this->getUser();

        $form = $this->createForm(PasswordUpdateType::class, $passwordUpdate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            // Vérifier que le oldPassword du formulaire soit identique à celui de l'utilisateur
            if (!$encoder->isPasswordValid($user, $passwordUpdate->getOldPassword())) 
            {
                $form->get('oldPassword')->addError(new FormError("Le mot de passe que vous avez tapé n'est pas votre mot de passe actuel"));
            }
            else 
            {
                $newPassword = $passwordUpdate->getNewPassword();
                $user->setPassword($encoder->encodePassword($user, $newPassword));

                $objectManager->persist($user);
                $objectManager->flush();

                $this->addFlash(
                    'success',
                    "Votre mot de passe a bien été enregistré !"
                );

                return $this->redirectToRoute('home');
            }
        }

        return $this->render("account/password.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * Permet d'afficher le prodult de l'utilisateur connecté
     * 
     * @Route("/account", name="account_index")
     * @Security("is_granted('ROLE_USER')", message="Vous devez vous connecter pour accéder à cette page !")
     *
     * @return Response
     */
    public function myAccount() 
    {
        return $this->render('fe_user/index.html.twig', [
            'user' => $this->getUser()
        ]);
    }
}
