<?php

namespace App\Form;

use App\Entity\FeUser;
use App\Form\ApplicationType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class RegistrationType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, $this->getConfiguration("Email", "Votre email..."))
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les deux mots de pass doivent correspondre.',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => ['label' => 'Mot de passe'],
                'second_options' => ['label' => 'Répétez votre mot de passe'],
            ])
            ->add('firstName', TextType::class, $this->getConfiguration("Nom", "Votre nom de famille..."))
            ->add('lastName', TextType::class, $this->getConfiguration("Prénom", "Votre prénom..."))
            ->add('picture', UrlType::class, $this->getConfiguration("Photo de profil", "URL de votre avatar..."))
            ->add('introduction', TextType::class, $this->getConfiguration("Introduciton", "Vous pouvez décrire quelques mots sur vous-même...", ['required' => false]))
            ->add('description', TextareaType::class, $this->getConfiguration("Description", "Vous pouvez même écrire tout un roman...", ['required' => false]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FeUser::class,
        ]);
    }
}
