<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountingMonthRepository")
 */
class AccountingMonth
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $activeMonth;

    /**
     * @ORM\Column(type="integer")
     */
    private $activeYear;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Operation", mappedBy="accountingMonth")
     */
    private $operations;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FeUser", inversedBy="accountingMonths")
     */
    private $author;

    public function __construct()
    {
        $this->operations = new ArrayCollection();
    }

    /**
     * Permet de retourner dans un format lisible l'intitulé du mois comptable
     *
     * @return string
     */
    public function getLiteral() 
    {
        $months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];

        return $months[$this->activeMonth - 1].' '.$this->activeYear;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getActiveMonth(): ?int
    {
        return $this->activeMonth;
    }

    public function setActiveMonth(int $activeMonth): self
    {
        $this->activeMonth = $activeMonth;

        return $this;
    }

    public function getActiveYear(): ?int
    {
        return $this->activeYear;
    }

    public function setActiveYear(int $activeYear): self
    {
        $this->activeYear = $activeYear;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * @return Collection|Operation[]
     */
    public function getOperations(): Collection
    {
        return $this->operations;
    }

    public function addOperation(Operation $operation): self
    {
        if (!$this->operations->contains($operation)) {
            $this->operations[] = $operation;
            $operation->setAccountingMonth($this);
        }

        return $this;
    }

    public function removeOperation(Operation $operation): self
    {
        if ($this->operations->contains($operation)) {
            $this->operations->removeElement($operation);
            // set the owning side to null (unless already changed)
            if ($operation->getAccountingMonth() === $this) {
                $operation->setAccountingMonth(null);
            }
        }

        return $this;
    }

    public function getAuthor(): ?FeUser
    {
        return $this->author;
    }

    public function setAuthor(?FeUser $author): self
    {
        $this->author = $author;

        return $this;
    }
}
