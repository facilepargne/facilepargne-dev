<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200107224957 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE operation (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, accounting_month_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, create_at DATETIME NOT NULL, amount DOUBLE PRECISION NOT NULL, family_member VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, INDEX IDX_1981A66D12469DE2 (category_id), INDEX IDX_1981A66DB2889016 (accounting_month_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE currency (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, short_name VARCHAR(100) NOT NULL, abbreviation VARCHAR(20) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE accounting_month (id INT AUTO_INCREMENT NOT NULL, author_id INT NOT NULL, active_month INT NOT NULL, active_year INT NOT NULL, create_at DATETIME NOT NULL, INDEX IDX_7EFB195CF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE establishment (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, genre VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fe_user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, picture VARCHAR(255) DEFAULT NULL, introduction VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, slug VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_3BA76907E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE financial_service (id INT AUTO_INCREMENT NOT NULL, establishment_id INT NOT NULL, label VARCHAR(255) NOT NULL, genre VARCHAR(255) NOT NULL, INDEX IDX_B35ED8B88565851 (establishment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, currency_id INT DEFAULT NULL, isin_code VARCHAR(50) NOT NULL, name VARCHAR(255) NOT NULL, short_code VARCHAR(255) NOT NULL, INDEX IDX_D34A04AD38248176 (currency_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transaction (id INT AUTO_INCREMENT NOT NULL, financial_service_id INT NOT NULL, category VARCHAR(255) NOT NULL, create_at DATETIME NOT NULL, price DOUBLE PRECISION NOT NULL, quantity INT DEFAULT NULL, amount DOUBLE PRECISION NOT NULL, description TINYTEXT DEFAULT NULL, INDEX IDX_723705D1683E59CF (financial_service_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE operation ADD CONSTRAINT FK_1981A66D12469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE operation ADD CONSTRAINT FK_1981A66DB2889016 FOREIGN KEY (accounting_month_id) REFERENCES accounting_month (id)');
        $this->addSql('ALTER TABLE accounting_month ADD CONSTRAINT FK_7EFB195CF675F31B FOREIGN KEY (author_id) REFERENCES fe_user (id)');
        $this->addSql('ALTER TABLE financial_service ADD CONSTRAINT FK_B35ED8B88565851 FOREIGN KEY (establishment_id) REFERENCES establishment (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD38248176 FOREIGN KEY (currency_id) REFERENCES currency (id)');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D1683E59CF FOREIGN KEY (financial_service_id) REFERENCES financial_service (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD38248176');
        $this->addSql('ALTER TABLE operation DROP FOREIGN KEY FK_1981A66DB2889016');
        $this->addSql('ALTER TABLE operation DROP FOREIGN KEY FK_1981A66D12469DE2');
        $this->addSql('ALTER TABLE financial_service DROP FOREIGN KEY FK_B35ED8B88565851');
        $this->addSql('ALTER TABLE accounting_month DROP FOREIGN KEY FK_7EFB195CF675F31B');
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D1683E59CF');
        $this->addSql('DROP TABLE operation');
        $this->addSql('DROP TABLE currency');
        $this->addSql('DROP TABLE accounting_month');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE establishment');
        $this->addSql('DROP TABLE fe_user');
        $this->addSql('DROP TABLE financial_service');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE transaction');
    }
}
